import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './cats/cats.controller';
import { IpMiddleware } from './middleware/ip.middleware';
import { LoggerMiddleware } from './middleware/logger.middleware';
import { UsersModule } from './users/users.module';
import { ormConfig } from '../ormconfig.js';

@Module({
  imports: [UsersModule, TypeOrmModule.forRoot(ormConfig)],
  controllers: [AppController, CatsController],
  providers: [AppService],
})

export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
      consumer
        .apply(LoggerMiddleware)
        .forRoutes('*')

      consumer
        .apply(IpMiddleware)
        .forRoutes('users')
  }
}
