import { Controller, Get, HttpStatus, Res, Param } from '@nestjs/common';
import { Response } from 'express';

@Controller('cats')
export class CatsController {
    @Get(':id')
    GetCats(@Res() response: Response, @Param() params){
        if (Math.random() * 10 > 5) {
            response.status(HttpStatus.OK)
            response.send(`Uhuuuuu deu sim, ${ params.id }`)
        } else {
            response.status(HttpStatus.UNAUTHORIZED)
            response.send("Ihhhh deu nao")
        }
    }
}
