import { NestMiddleware } from "@nestjs/common";
import { Response, Request, NextFunction } from "express";
export declare class IpMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction): void;
}
