import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'

export const ormConfig:PostgresConnectionOptions = {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: 5432,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: ["dist/src/**/entities/*.entity.js"],
    // migrations: ["dist/database/migrations/*{.ts,.js}"],
    // migrationsRun: true,
    synchronize: true
}